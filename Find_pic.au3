#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Compression=4
#AutoIt3Wrapper_Res_Comment=find_pic5
#AutoIt3Wrapper_Res_Fileversion=5.0
#AutoIt3Wrapper_Res_LegalCopyright=Vint
#AutoIt3Wrapper_Res_Language=1049
#AutoIt3Wrapper_Res_requestedExecutionLevel=None
#AutoIt3Wrapper_Res_Field=Version|5.0
#AutoIt3Wrapper_Res_Field=Build|2019.12.18
#AutoIt3Wrapper_Res_Field=Coded by|Vint
#AutoIt3Wrapper_Res_Field=Compile date|%longdate% %time%
#AutoIt3Wrapper_Res_Field=AutoIt Version|%AutoItVer%
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <UpDownConstants.au3>
#include <File.au3>
#include <Array.au3>
#include <Sound.au3>
#include <GDIPlus.au3>

Global $Form1, $name, $transp, $percent, $colorm, $find, $aCoord[500][2], $Horz[500], $Vert[500]
Global $FileStatus, $ButtonStart, $ButtonHide, $name_full = '', $SoundState, $sSound, $Vual, $color1 = 0xFF0000, $sound2[2]
Global $first = True, $set_file = False, $mode = 0, $ini = @ScriptDir & "\find_pic.ini"

Global $colors[12] = ["FFFF00","00FFFF","5fc700","854b00","76e07c","FFAC00","FFFF66","ff0000","FFFFFF","850044","B56AFF","56B1FF"]
Global $hForm, $hLabel, $hParent
Global $num = 0, $gr = 0, $mode_gr = 1, $ColorStart, $ColorEnd, $ExitColor, $hTimerCol
Global $red_from, $green_from, $blue_from, $red_to, $green_to, $blue_to, $red_add, $green_add, $blue_add, $red_temp, $green_temp, $blue_temp
Global $speed_color = 5


FileInstall("find_pic.cms", @ScriptDir & "\find_pic.cms")
FileInstall("find_pic.ini", @ScriptDir & "\find_pic.ini")
FileInstall("Vint_avatar_11_64x64.jpg", @TempDir & "\Vint_avatar_11_64x64.jpg")
FileInstall("cancel4.mp3", @TempDir & "\cancel4.mp3")
FileInstall("��_�������.mp3", @TempDir & "\��_�������.mp3")
FileInstall("�������.mp3", @TempDir & "\�������.mp3")
FileInstall("over_100.mp3", @TempDir & "\over_100.mp3")
FileInstall("1.mp3", @TempDir & "\1.mp3")
FileInstall("2.mp3", @TempDir & "\2.mp3")
FileInstall("3.mp3", @TempDir & "\3.mp3")
FileInstall("4.mp3", @TempDir & "\4.mp3")
FileInstall("5.mp3", @TempDir & "\5.mp3")
FileInstall("6.mp3", @TempDir & "\6.mp3")
FileInstall("7.mp3", @TempDir & "\7.mp3")
FileInstall("8.mp3", @TempDir & "\8.mp3")
FileInstall("9.mp3", @TempDir & "\9.mp3")
FileInstall("10.mp3", @TempDir & "\10.mp3")
FileInstall("11.mp3", @TempDir & "\11.mp3")
FileInstall("12.mp3", @TempDir & "\12.mp3")
FileInstall("13.mp3", @TempDir & "\13.mp3")
FileInstall("14.mp3", @TempDir & "\14.mp3")
FileInstall("15.mp3", @TempDir & "\15.mp3")
FileInstall("16.mp3", @TempDir & "\16.mp3")
FileInstall("17.mp3", @TempDir & "\17.mp3")
FileInstall("18.mp3", @TempDir & "\18.mp3")
FileInstall("19.mp3", @TempDir & "\19.mp3")
FileInstall("20.mp3", @TempDir & "\20.mp3")
FileInstall("30.mp3", @TempDir & "\30.mp3")
FileInstall("40.mp3", @TempDir & "\40.mp3")
FileInstall("50.mp3", @TempDir & "\50.mp3")
FileInstall("60.mp3", @TempDir & "\60.mp3")
FileInstall("70.mp3", @TempDir & "\70.mp3")
FileInstall("80.mp3", @TempDir & "\80.mp3")
FileInstall("90.mp3", @TempDir & "\90.mp3")
FileInstall("100.mp3", @TempDir & "\100.mp3")

FileInstall("Cloud.mp3", @TempDir & "\Cloud.mp3")
$sSound = _SoundOpen (@TempDir & "\Cloud.mp3")


_ReadingINI($ini)
_ReadCoord()
$find = 0
;$name = ""
;$set_file = True  ; Test

;~ HotKeySet("{SPACE}", "_Mode")

While 1
    _MainForm()
    _ResultClose()
    _WriteINI($ini)
    _Run()
    _ReadingINI($ini)
    _ReadCoord()
    _Result()
WEnd

Func _MainForm()
    Dim $FileStatus, $ButtonStart, $name,  $transp, $percent, $colorm, $find
    Local $Form1, $Input_name, $Input_transp, $Input_percent, $Input_colormode, $ButtonFile, $ButtonMore
    Local $hTimerWait, $nMsg

    $SoundState = False

    $Form1 = GUICreate("����� ��������", 440, 85, -1, -1)
    GUICtrlCreateGroup("", 3, 3, 434, 79)
    GUICtrlCreateGroup("", -99, -99, 1, 1)
    ;GUICtrlCreateLabel("��� �����", 80, 10, 60, 17)
    GUICtrlCreateLabel("����", 40, 12, 30, 17)

    $FileStatus = GUICtrlCreateLabel("...", 90, 12, 60, 17)
    GUICtrlSetColor(-1, 0xFF0000)

    GUICtrlCreateLabel(".bmp", 200, 34, 30, 17)
    GUICtrlSetFont(-1, 8.5, 700, 0, "Arial")
    GUICtrlCreateLabel("������������", 238, 12, 72, 17)
    GUICtrlCreateLabel("%", 338, 12, 12, 17)
    GUICtrlCreateLabel("colormode", 378, 12, 53, 17)

    $ButtonFile = GUICtrlCreateButton("...", 8, 30, 20, 20)
    $Input_name = GUICtrlCreateInput($name, 28, 30, 170, 20)
    $Input_transp = GUICtrlCreateInput($transp, 242, 30, 65, 20) ; "16777215"
    GUICtrlSetLimit(-1, 8)
    $Input_percent = GUICtrlCreateInput($percent, 324, 30, 42, 20)
    GUICtrlSetLimit(-1, 3)
    GUICtrlCreateUpdown(-1, $UDS_NOTHOUSANDS) ; ��� ����������� � 1000
    GUICtrlSetLimit(-1, 100, 0)
    $Input_colormode = GUICtrlCreateInput($colorm, 390, 30, 30, 20)
    GUICtrlSetLimit(-1, 1)
    GUICtrlCreateUpdown(-1, $UDS_NOTHOUSANDS) ; ��� ����������� � 1000
    GUICtrlSetLimit(-1, 8, 0)
    $ButtonStart = GUICtrlCreateButton("�����!", 7, 55, 100, 25)
    _FileStatus()

    If $first = False Then
        If $find > 0 Then
            GUICtrlCreateLabel("�������:", 120, 62, 50, 17)
            GUICtrlSetFont(-1, 8.5, 700, 0, "Arial")

            If $find >= 500 Then
                GUICtrlCreateLabel("500+", 175, 62, 50, 17)
            Else
                GUICtrlCreateLabel($find, 175, 62, 50, 17)
            EndIf
            GUICtrlSetFont(-1, 8.5, 700, 0, "Arial")
            GUICtrlSetColor(-1, 0x0000FF)
            GUICtrlCreateLabel("��.", 205, 62, 20, 17)
            GUICtrlSetFont(-1, 8.5, 700, 0, "Arial")

            $ButtonHide = GUICtrlCreateButton("������", 225, 57, 50, 20)
        Else
            GUICtrlCreateLabel("�� �������!", 140, 62, 70, 17)
            GUICtrlSetFont(-1, 8.5, 700, 0, "Arial")
            GUICtrlSetColor(-1, 0xFF0000)
        EndIf
    EndIf

    GUICtrlCreateLabel("X,Y:", 316, 62, 20, 17)
    GUICtrlSetFont(-1, 8.5, 700, 0, "Arial")

    $ButtonMore = GUICtrlCreateButton("more", 400, 57, 30, 20)

    If $find = 0 Then
        GUICtrlCreateLabel(" ----   ----", 340, 62, 50, 17)
    Else
        GUICtrlCreateLabel($aCoord[0][0]&","&$aCoord[0][1], 340, 62, 60, 17)
    EndIf

    If $find < 2 Then
        GUICtrlSetState($ButtonMore,$GUI_DISABLE)
    Else
        GUICtrlSetState($ButtonMore,$GUI_ENABLE)
    EndIf

    GUICtrlSetFont(-1, 8.5, 700, 0, "Arial")
    GUICtrlSetColor(-1, 0x0000FF)

    GUISetState(@SW_SHOW, $Form1)
    GUISwitch($Form1)
    $hTimerWait = TimerInit()

    While 1
        $nMsg = GUIGetMsg()
        Select
            Case $nMsg = $ButtonHide
                $mode = 4
                _ResultHide()
                HotKeySet("{SPACE}")
            Case $nMsg = $Input_transp or $nMsg = $Input_percent or $nMsg = $Input_colormode
                If $SoundState = False Then $hTimerWait = TimerInit()
                _StopSound()
            Case $nMsg = $Input_name
                If $SoundState = False Then $hTimerWait = TimerInit()
                _StopSound()

                $name = GUICtrlRead($Input_name)
                $name = _CheckName($name)
                If $name <> "" Then
                    If StringMid ($name, StringLen($name)-3) = ".bmp" Then $name = StringMid ($name, 1, StringLen($name)-4)
                    GUICtrlSetData ($Input_name, $name)
                EndIf
            Case $nMsg = $ButtonFile
                If $SoundState = False Then $hTimerWait = TimerInit()
                _StopSound()

                $name = FileOpenDialog("�������� ����...", @ScriptDir & "\", "���� ����������� (*.bmp)",1,"",$Form1)
                If @error Then
                    MsgBox(4096, "", "���� �� ������", 1)
                Else
                    ;MsgBox(4096, "", "���������� " & StringMid ($name, StringLen($name)-3), 2)
                    If StringMid ($name, StringLen($name)-3) <> ".bmp" Then
                        MsgBox(4096, "", "���� �� ������� bmp", 1)
                    Else
                        $name = StringMid ($name, 1, StringLen($name)-4)
                        ;MsgBox(4096, "", "��� ���������� " & $name, 2)
                        $name = _CheckName($name)
                        If $name <> "" Then
                            GUICtrlSetData ($Input_name, $name)
                        EndIf
                    EndIf
                EndIf
            Case $nMsg = $ButtonMore
                If $SoundState = False Then $hTimerWait = TimerInit()
                _ArrayDisplay( $aCoord, "��������� ����������", $find-1, 0, '', '|', '�|X|Y')
            Case $nMsg = $ButtonStart
                If $SoundState = False Then $hTimerWait = TimerInit()
                _StopSound()

                $name = GUICtrlRead($Input_name)
                $transp = GUICtrlRead($Input_transp)
                $percent = GUICtrlRead($Input_percent)
                $colorm = GUICtrlRead($Input_colormode)
                GUIDelete($Form1)
                $first = False
                ExitLoop
            Case $nMsg = $GUI_EVENT_CLOSE
                _StopSound()
                GUIDelete($Form1)
                _ResultClose()
                _EndProgramm()
                Exit
        EndSelect

        ; ������ �����
        If $SoundState = False Then
            If TimerDiff($hTimerWait) > 30000 Then
                _SoundPlay($sSound,0)
                $SoundState = True
            EndIf
        EndIf

        ; ������ ��������
        If $find > 0 And ($mode = 2 Or $mode = 3) Then _Rainbow()
    WEnd
EndFunc

Func _Rainbow()
    Local $i

    If $mode_gr = 1 Then
        $ColorStart = $colors[$num]
        If $num = 11 Then
            $ColorEnd = $colors[0]
        Else
            $ColorEnd = $colors[$num+1]
        EndIf
        $red_from = Dec(StringMid($ColorStart, 1, 2))
        $green_from = Dec(StringMid($ColorStart, 3, 2))
        $blue_from = Dec(StringMid($ColorStart, 5, 2))
        $red_to = Dec(StringMid($ColorEnd, 1, 2))
        $green_to = Dec(StringMid($ColorEnd, 3, 2))
        $blue_to = Dec(StringMid($ColorEnd, 5, 2))
        $red_temp = $red_from
        $green_temp = $green_from
        $blue_temp = $blue_from
        $red_add = ($red_from-$red_to)/$speed_color
        $green_add = ($green_from-$green_to)/$speed_color
        $blue_add = ($blue_from-$blue_to)/$speed_color
        $gr = 0
        $mode_gr = 2
    EndIf

    ; ��������
    If $mode_gr = 2 Then
        If $gr <= $speed_color Then
            $ExitColor = "0x" & Hex(Int($red_temp), 2) & Hex(Int($green_temp), 2) & Hex(Int($blue_temp), 2)
            ; ������ ����
            $find_temp = $find
            If $find > 500 Then $find_temp = 500
            For $i=0 To ($find_temp-1)
                GUISetBkColor($ExitColor, $Horz[$i])
                GUISetBkColor($ExitColor, $Vert[$i])
            Next
            $red_temp -= $red_add
            $green_temp -= $green_add
            $blue_temp -= $blue_add
            $gr +=1
            Sleep(10)
        Else
            $num += 1
            If $num > 11 Then $num = 0
            $mode_gr = 3
            $hTimerCol = TimerInit()
        EndIf
    EndIf

    If $mode_gr = 3 And TimerDiff($hTimerCol) > 500 Then
        $mode_gr = 1
    EndIf
EndFunc

Func _Result()
    Dim $find
    Local $i, $find_temp, $trans = 255

    _ItogMsb()
    If $find = 0 Then Return

    HotKeySet("{SPACE}", "_Mode")
    $Vual = GUICreate("", @DesktopWidth, @DesktopHeight, 0, 0, BitOR($WS_DISABLED, $WS_POPUP), $WS_EX_TOOLWINDOW)
    GUISetBkColor(0x000000)
    WinSetTrans($Vual, '', 100)
    ;GUISetState(@SW_SHOWNOACTIVATE, $Vual)

    $find_temp = $find
    If $find > 500 Then $find_temp = 500
    For $i=0 To ($find_temp-1)
        ;MsgBox(4096, "", $aCoord[$i][0] & " " & $aCoord[$i][1], 2)
        $Horz[$i] = GUICreate("", 21, 1, $aCoord[$i][0]-10, $aCoord[$i][1], $WS_POPUP, BitOR($WS_EX_TOPMOST, $WS_EX_TOOLWINDOW))
        GUISetBkColor($color1, $Horz[$i])
        WinSetTrans($Horz[$i], '', $trans)
        GUISetState(@SW_SHOWNOACTIVATE, $Horz[$i])

        $Vert[$i] = GUICreate("", 1, 21, $aCoord[$i][0], $aCoord[$i][1]-10, $WS_POPUP, BitOR($WS_EX_TOPMOST, $WS_EX_TOOLWINDOW))
        GUISetBkColor($color1, $Vert[$i])
        WinSetTrans($Vert[$i], '', $trans)
        GUISetState(@SW_SHOWNOACTIVATE, $Vert[$i])
    Next
    $mode = 1
    $mode_gr = 3
    $num = 0
    $gr = 0
EndFunc

Func _ItogMsb()
    Local $text, $sound, $sound3, $sound4, $FormItog, $ButtonOK, $hTimerItog, $nMsg2

    $text = "������� �����������: " & $find
    $sound = _SoundOpen (@TempDir & "\�������.mp3")
    If $find = 0 Then
        $text = "������ �� �������!"
        $sound = _SoundOpen (@TempDir & "\��_�������.mp3")
    ElseIf $find > 500 Then
        $text = "������� �����������: 500+"
        $sound = _SoundOpen (@TempDir & "\over_100.mp3")
    Else
        $sound2 = _Numerik($find)
        $sound3 = _SoundOpen ($sound2[0])
        $sound4 = _SoundOpen ($sound2[1])
    EndIf

    $FormItog = GUICreate("����� ��������", 190, 90, -1, -1)
    GUICtrlCreateLabel($text, 10, 24, 170, -1, $SS_CENTER)
    $ButtonOK = GUICtrlCreateButton("OK", 70, 54, 50, 20)
    GUISetState(@SW_SHOW, $FormItog)
    $hTimerItog = TimerInit()

    _SoundPlay($sound,1)
    Sleep(100)
    If $find = 0 Or $find > 100 Then
    ElseIf $find < 21 Or Mod($find, 10) = 0 Then
        _SoundPlay($sound3,1)
    Else
        _SoundPlay($sound3,0)
        Sleep(700)
        _SoundPlay($sound4,1)
    EndIf
    _SoundClose($sound)
    _SoundClose($sound3)
    _SoundClose($sound4)

    While 1
        If TimerDiff($hTimerItog) > 2000 Then ExitLoop
        $nMsg2 = GUIGetMsg()
        Select
            Case $nMsg2 = $GUI_EVENT_CLOSE or $nMsg2 = $ButtonOK
                ExitLoop
        EndSelect
    WEnd
    GUIDelete($FormItog)
EndFunc

Func _Numerik($N)
    Local $Out[2]
    If $N < 21 Or Mod($N, 10) = 0 Then
        $Out[0] = @TempDir & "\" & $N & ".mp3"
        ;MsgBox(4096, "", "$N = " & $N, 2)
    Else
        $Out[0] = @TempDir & "\" & Int($N/10)*10 & ".mp3"
        $Out[1] = @TempDir & "\" & $N - Int($N/10)*10 & ".mp3"
        ;MsgBox(4096, "", "ltcznrb = " & Int($N/10)*10 & "  ������� = " & $N - Int($N/10)*10, 2)
    EndIf
    Return($Out)
EndFunc

Func _Mode()
    Dim $mode

    If $mode = 0 Then Return

    If $mode = 1 Then
        $mode = 2
    ElseIf $mode = 2  Then
        $mode = 3
        GUISetState(@SW_SHOWNOACTIVATE, $Vual)
    ElseIf $mode = 3  Then
        $mode = 4
        _ResultHide()
    ElseIf $mode = 4  Then
        $mode = 1
        _ResultRed()
        _ResultShow()
    Else
        $mode = 0
        _ResultClose()
    EndIf
EndFunc

Func _ResultHide()
    Dim $find
    Local $i, $find_temp

    If $find = 0 Then Return
    $find_temp = $find
    If $find > 500 Then $find_temp = 500
    For $i=0 To ($find_temp-1)
        GUISetState(@SW_HIDE, $Horz[$i])
        GUISetState(@SW_HIDE, $Vert[$i])
    Next
    GUISetState(@SW_HIDE, $Vual)
EndFunc

Func _ResultShow()
    Dim $find
    Local $i, $find_temp

    If $find = 0 Then Return
    ;GUISetState(@SW_SHOWNOACTIVATE, $Vual)
    $find_temp = $find
    If $find > 500 Then $find_temp = 500
    For $i=0 To ($find_temp-1)
        GUISetState(@SW_SHOWNOACTIVATE, $Horz[$i])
        GUISetState(@SW_SHOWNOACTIVATE, $Vert[$i])
    Next
;~     GUISwitch($Form1)
    $mode_gr = 3
    $num = 0
    $gr = 0
EndFunc

Func _ResultRed()
    Dim $find
    Local $i, $find_temp

    If $find = 0 Then Return
    $find_temp = $find
    If $find > 500 Then $find_temp = 500
    For $i=0 To ($find_temp-1)
        GUISetBkColor($color1, $Horz[$i])
        GUISetBkColor($color1, $Vert[$i])
    Next
EndFunc

Func _ResultClose()
    Dim $find
    Local $i, $find_temp

    If $find = 0 Then Return
    $find_temp = $find
    If $find > 500 Then $find_temp = 500
    For $i=0 To ($find_temp-1)
        GUIDelete($Horz[$i])
        GUIDelete($Vert[$i])
    Next
    GUIDelete($Vual)
EndFunc

Func _Run()
    ShellExecuteWait(@ScriptDir & '\find_pic.cms')
EndFunc

Func _FileStatus()
    Dim $set_file, $FileStatus, $name_full
    Local $hBitmap1, $Width, $Height

    If $set_file Then
        _GDIPlus_Startup()
        $hBitmap1 = _GDIPlus_BitmapCreateFromFile($name_full)
        $Width =_GDIPlus_ImageGetWidth ($hBitmap1)
        $Height = _GDIPlus_ImageGetHeight($hBitmap1)
        _GDIPlus_BitmapDispose($hBitmap1)
        GUICtrlSetData($FileStatus, $Width & "x" & $Height)
        GUICtrlSetFont($FileStatus, 8.5, 700, 0, "Arial")
        GUICtrlSetColor($FileStatus, 0x35A136)
        GUICtrlSetState($ButtonStart,$GUI_ENABLE)
    Else
        GUICtrlSetData($FileStatus, "�� �����!")
        GUICtrlSetColor($FileStatus, 0xFF0000)
        GUICtrlSetState($ButtonStart,$GUI_DISABLE)
    EndIf
EndFunc

Func _CheckName($name_temp)
    Dim $set_file, $ini, $name_full

    if StringInStr ($name_temp, @ScriptDir) Then
        $name_full = $name_temp & ".bmp"
        $name_temp = StringReplace ($name_temp, @ScriptDir & "\", "" , 1)
    Else
        $name_full = $name_temp & ".bmp"
    EndIf
    ;MsgBox(4096,"","$name_temp = "&$name_temp&@LF&@LF&" $name_full = "&$name_full, 0)

    If FileExists ($name_full) Then
        $set_file = True
        _FileStatus()
        IniWrite($ini, "default", "name", $name_temp & ".bmp")
        Return $name_temp
    Else
        MsgBox(4096,"","���� " & $name_temp & " �� ����������", 0)
        Return ""
    EndIf
EndFunc

Func _ReadingINI($filename)
    Dim $name, $transp, $percent, $colorm, $find
    Local $Section

    $Section = IniReadSectionNames($filename)
    If @error Then
        MsgBox(4096, "", "��������� ������, �������� ����������� ��� �� ������ INI-����.")
        Exit
    Else
        $name = IniRead($filename, "default", "name", "File_name")
        If StringMid ($name, StringLen($name)-3) = ".bmp" Then $name = StringMid ($name, 1, StringLen($name)-4)
        $transp = IniRead($filename, "default", "transp", "-1")
        $percent = IniRead($filename, "default", "percent", "100")
        $colorm = IniRead($filename, "default", "colorm", "0")
        $find = IniRead($filename, "default", "find", "0")
    EndIf
EndFunc

Func _WriteINI($filename)
    Dim $name, $transp, $percent, $colorm, $find
    Local $contr = 1, $Temp

    $Temp = IniWrite( $filename, "default", "name", $name & ".bmp")
    If $Temp = 0 Then $contr = 0
    $Temp = IniWrite( $filename, "default", "transp", $transp)
    If $Temp = 0 Then $contr = 0
    $Temp = IniWrite( $filename, "default", "percent", $percent)
    If $Temp = 0 Then $contr = 0
    $Temp = IniWrite( $filename, "default", "colorm", $colorm)
    If $Temp = 0 Then $contr = 0
    $Temp = IniWrite( $filename, "default", "find", $find)
    If $Temp = 0 Then $contr = 0
    $Temp = IniWrite( $filename, "default", "state", "0")
    If $Temp = 0 Then $contr = 0
    If $contr = 0 Then
        MsgBox(4096,"","������ ����������!"&@LF&"�������� INI-���� ���������� ��� ������.", 0)
        Exit
    Else
;~         MsgBox(4096,"����������","��������� ���������", 3)
;~         $changes = False
    EndIf
    If FileExists (@ScriptDir & "\find_pic.txt") Then FileDelete(@ScriptDir & "\find_pic.txt")
EndFunc

Func _ReadCoord()
    Dim $find
    Local $i=0

    If Not FileExists (@ScriptDir & "\find_pic.txt") or $find = 0 Then
        $find = 0
        Return
    EndIf

    $hFile = FileOpen ( @ScriptDir & "\find_pic.txt", 0  )
    If $hFile = -1 Then
        MsgBox(4096, "������", "���������� ������� ���� � ������������.")
        $find = 0
        Return
    EndIf

    ;----------------------------------------------------------
;~     $lines = _FileCountLines(@ScriptDir & "\find_pic.txt")
;~     if $lines/2 < $find Then
;~         MsgBox(4096, "������", "�� ������� ��������� � ����� ��� " & $find & " ��������� ��������")
;~         ; Exit
;~     EndIf
;~     MsgBox(4096, "", "����� � ����� = " & $lines)
    ;----------------------------------------------------------

    While $i < $find
        $aCoord[$i][0] = FileReadLine($hFile)
        If @error = -1 Then
            MsgBox(4096, "������", "�� ������� ��������� � ����� ��� " & $find - $i & " ��������� ��������. X")
            ExitLoop
        EndIf
        $aCoord[$i][1] = FileReadLine($hFile)
        If @error = -1 Then
            MsgBox(4096, "������", "�� ������� ��������� � ����� ��� " & $find - $i & " ��������� ��������. Y")
            ExitLoop
        EndIf
        $i += 1
        If $i > 499 Then ExitLoop
    WEnd
    FileClose ($hFile)
EndFunc

Func _StopSound()
    If $SoundState = True And _SoundStatus($sSound) = "playing" Then _SoundStop($sSound)
EndFunc

Func _EndProgramm()
    Local $FormEnd, $i, $hParent, $SoundCancel

    $hParent  = GUICreate('')
    $FormEnd = GUICreate("� ���������", 200, 160, -1, -1, BitOR($WS_DISABLED, $WS_POPUP), $WS_EX_TOPMOST, $hParent)
    GUICtrlCreatePic(@TempDir & "\Vint_avatar_11_64x64.jpg", 68, 8, 64, 64)
    GUICtrlCreateLabel("Autor Vint", 68, 80, 70, 20)
    GUICtrlSetFont(-1, 10, 800, 0, "MS Sans Serif")
    GUICtrlSetColor(-1, 0x800080)
    GUICtrlCreateLabel("version 5.0 of 18.12.2019", 50, 95, 125, 17)
    GUICtrlSetFont(-1, 8, 400, 0, "Tahoma")
    GUICtrlCreateLabel("���������� �����", 54, 116, 100, 17)
    GUICtrlCreateLabel("��� Clickermann", 64, 130, 82, 17)
    WinSetTrans($FormEnd, '', 0) ; ������������ 30
    GUISetBkColor(0xDCF2CA, $FormEnd)
    GUISetState(@SW_SHOW)

    For $i = 0 To 255 Step 8
        WinSetTrans($FormEnd, '', $i)
        Sleep(1)
    Next
    $i = 1024
    While $i >= 0
        If $i < 256 Then WinSetTrans($FormEnd, '', $i)

        $nMsg = GUIGetMsg()
        Switch $nMsg
            Case $GUI_EVENT_CLOSE
                Exit
        EndSwitch
        Sleep(1)
        $i -= 8
    WEnd

    _SoundClose($sSound)
    $SoundCancel = _SoundOpen (@TempDir & "\cancel4.mp3")
    _SoundPlay($SoundCancel,1)
    _SoundClose($SoundCancel)

    FileDelete(@TempDir & "\cancel4.mp3")
    FileDelete(@TempDir & "\Cloud.mp3")
    FileDelete(@TempDir & "\��_�������.mp3")
    FileDelete(@TempDir & "\�������.mp3")
    FileDelete(@TempDir & "\over_100.mp3")

    If @Compiled Then
        FileDelete(@ScriptDir & "\find_pic.cms")
        FileDelete(@ScriptDir & "\find_pic.ini")
    EndIf

    For $j=0 To 20
        FileDelete(@TempDir & "\" & $j & ".mp3")
    Next
    For $j=30 To 100 Step 10
        FileDelete(@TempDir & "\" & $j & ".mp3")
    Next
    FileDelete(@TempDir & "\Vint_avatar_11_64x64.jpg")
    Exit
EndFunc


;~ $sSound_1 = _SoundOpen (@TempDir & "\��_�������.mp3")
;~ _SoundPlay($sSound_1,1)
;~ _SoundClose($sSound_1)


;~ WAITMS(300)
;~ $name = INIREAD("find_pic.ini", "name", "default")
;~ $transp = INIREAD("find_pic.ini", "transp", "default")
;~ $percent = INIREAD("find_pic.ini", "percent", "default")
;~ $colorm = INIREAD("find_pic.ini", "colorm", "default")
;~ WAITMS(100)

;~ $XX = $_xmouse
;~ $YY = $_ymouse
;~ MOVE($_xmax,0)
;~ WAITMS(20)
;~ GETSCREEN
;~ MOVE($XX,$YY)
;~ IF($colorm ! 0)
;~    COLORMODE($colorm, 0,0,$_xmax,$_ymax)
;~ END_IF
;~ SCANPICTURE($arr,0,0,$_xmax,$_ymax,$name,$transp,$percent)

;~ INIWRITE("find_pic.ini", "find", (ARRSIZE($arr_find)/2))
;~ WAITMS(30)
;~ TFWRITEARR ("find_pic.txt", $arr)
;~ WAITMS(30)
;~ INIWRITE("find_pic.ini", "state", "1")
;~ WAITMS(50)
;~ HALT(1)