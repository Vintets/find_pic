#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <UpDownConstants.au3>
#include <File.au3>
#include <Array.au3>
#include <Sound.au3>
#include <GDIPlus.au3>

Global $sound2[2]
FileInstall("��_�������.mp3", @TempDir & "\��_�������.mp3")
FileInstall("�������.mp3", @TempDir & "\�������.mp3")
FileInstall("over_100.mp3", @TempDir & "\over_100.mp3")
For $j=0 To 20
	FileInstall($j & ".mp3", @TempDir & "\" & $j & ".mp3")
Next
For $j=30 To 100 Step 10
	FileInstall($j & ".mp3", @TempDir & "\" & $j & ".mp3")
Next
Sleep(1000)

Func _ItogMsb($find)
	Local $text, $sound, $FormItog, $ButtonOK, $hTimerItog, $nMsg2, $sout

	$text = "������� �����������: " & $find
	$sound = @TempDir & "\�������.mp3"
	If $find = 0 Then
		$text = "������ �� �������!"
		$sound = @TempDir & "\��_�������.mp3"
	ElseIf $find > 100 Then
		$text = "������� �����������: 100+"
		$sound = @TempDir & "\over_100.mp3"
	Else
		$sound2 = _Numerik($find)
	EndIf

	$FormItog = GUICreate("����� ��������", 190, 90, -1, -1)
	GUICtrlCreateLabel($text, 10, 24, 170, -1, $SS_CENTER)
	$ButtonOK = GUICtrlCreateButton("OK", 70, 54, 50, 20)
	GUISetState(@SW_SHOW, $FormItog)
	$hTimerItog = TimerInit()

;~ 	_SoundPlay($sound,1)
	Sleep(100)
	If $find = 0 Or $find > 100 Then
		_SoundPlay($sound,1)
	ElseIf $find < 21 Or $find = 30 Or $find = 40 Or $find = 50 Or $find = 60 Or $find = 70 Or $find = 80 Or $find = 90 Or $find = 100 Then
		_SoundPlay($sound2[0],1)
	Else
		_SoundPlay($sound2[0],0)
		Sleep(650)
		_SoundPlay($sound2[1],1)
	EndIf

	While 1
		If TimerDiff($hTimerItog) > 2000 Then ExitLoop
		$nMsg2 = GUIGetMsg()
		Select
			Case $nMsg2 = $GUI_EVENT_CLOSE or $nMsg2 = $ButtonOK
				ExitLoop
		EndSelect
	WEnd
	GUIDelete($FormItog)
EndFunc

Func _Numerik($N)
	Local $Out[2]
	If $N < 21 Or $N = 30 Or $N = 40 Or $N = 50 Or $N = 60 Or $N = 70 Or $N = 80 Or $N = 90 Or $N = 100 Then
		$Out[0] = @TempDir & "\" & $N & ".mp3"
		;MsgBox(4096, "", "$N = " & $N, 2)
	Else
		$Out[0] = @TempDir & "\" & Int($N/10)*10 & ".mp3"
		$Out[1] = @TempDir & "\" & $N - Int($N/10)*10 & ".mp3"
		;MsgBox(4096, "", "ltcznrb = " & Int($N/10)*10 & "  ������� = " & $N - Int($N/10)*10, 2)
	EndIf
	Return($Out)
EndFunc

;~ $find_ = 98
;~ _ItogMsb($find_)
;~ Sleep(1000)

For $i = 0 To 101
	_ItogMsb($i)
	;Sleep(1000)
Next

